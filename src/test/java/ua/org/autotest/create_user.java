package ua.org.autotest;


import org.testng.annotations.*;

import static ua.org.autotest.poker_player.generate_player;

public class create_user extends base_test{
    @BeforeSuite
    void setup()
    {
        super.setup();
    }

    @BeforeTest
    public void login()
    {
        super.login();
    }


    @Test
    public void create_user_test()
    {
        poker_player player = generate_player();

        tester_.add_user(player);
        tester_.find_user(player);
        tester_.validate_user_fields(player);
        tester_.delete_user(player);
    }


    @AfterTest
    void logout()
    {
        super.logout();
    }

    @AfterSuite
    void close_browser()
    {
        super.close_browser();
    }
}
