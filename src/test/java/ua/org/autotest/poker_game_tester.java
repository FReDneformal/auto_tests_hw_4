package ua.org.autotest;
import junit.framework.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class poker_game_tester
{
    public static class website_credentials
    {
        public String login;
        public String password;
    }

    public poker_game_tester(WebDriver web_driver)
    {
        chrome_driver_ = web_driver;
    }

    public void open_website(String url)
    {
        chrome_driver_.get(url);

        // delay();
    }


    public void login_user(website_credentials credentials)
    {
        WebElement username = chrome_driver_.findElement(By.name("username"));
        WebElement password = chrome_driver_.findElement(By.name("password"));
        WebElement login_button = chrome_driver_.findElement(By.name("logIn"));

        username.sendKeys(credentials.login);
        password.sendKeys(credentials.password);

        login_button.click();

        Assert.assertEquals(chrome_driver_.getTitle(),"Players");

        //delay();
    }

    public void add_user(poker_player player)
    {
        WebElement insertButton = chrome_driver_.findElement(By.linkText("Insert"));

        insertButton.click();

        WebElement login_field = chrome_driver_.findElement(By.name("us_login"));
        WebElement email_field = chrome_driver_.findElement(By.name("us_email"));
        WebElement password_field = chrome_driver_.findElement(By.name("us_password"));
        WebElement confirm_password_field = chrome_driver_.findElement(By.name("confirm_password"));
        WebElement first_name_field = chrome_driver_.findElement(By.name("us_fname"));
        WebElement last_name_field = chrome_driver_.findElement(By.name("us_lname"));
        WebElement city_field = chrome_driver_.findElement(By.name("us_city"));
        WebElement address_field = chrome_driver_.findElement(By.name("us_address"));
        WebElement phone_field = chrome_driver_.findElement(By.name("us_phone"));
        WebElement submit_button = chrome_driver_.findElement(By.name("Submit"));

        login_field.sendKeys(player.login);
        email_field.sendKeys(player.email);
        password_field.sendKeys(player.password_confirmed);
        confirm_password_field.sendKeys(player.password_confirmed);
        first_name_field.sendKeys(player.first_name);
        last_name_field.sendKeys(player.last_name);
        city_field.sendKeys(player.city);
        address_field.sendKeys(player.address);
        phone_field.sendKeys(player.phone);

        submit_button.click();

        Assert.assertEquals(chrome_driver_.getTitle(),"Players");

        //delay();
    }

    public void  find_user(poker_player player){
        WebElement login_for_search = chrome_driver_.findElement(By.name("login"));
        WebElement search_button = chrome_driver_.findElement(By.xpath("//button[text()='Search']"));

        login_for_search.sendKeys(player.login);

        search_button.click();

        //delay();

        WebDriverWait wait = new WebDriverWait(chrome_driver_, 10);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@class =\"sprite sprite-edit\"]")));


        WebElement edit_button = chrome_driver_.findElement(By.xpath("//a[@class =\"sprite sprite-edit\"]"));

        edit_button.click();

        //delay();
    }

    public void validate_user_fields(poker_player player)
    {
        WebElement email_test = chrome_driver_.findElement(By.name("us_email"));
        String input_text_email = email_test.getAttribute("value");

        Assert.assertEquals(input_text_email, player.email);

        WebElement first_name_test = chrome_driver_.findElement(By.name("us_fname"));
        String input_text_name = first_name_test.getAttribute("value");
        Assert.assertEquals(input_text_name, player.first_name);

        WebElement lname_test = chrome_driver_.findElement(By.name("us_lname"));
        String input_text_last_name = lname_test.getAttribute("value");
        Assert.assertEquals(input_text_last_name, player.last_name);

        WebElement city_test = chrome_driver_.findElement(By.name("us_city"));
        String input_text_city = city_test.getAttribute("value");
        Assert.assertEquals(input_text_city, player.city);

        WebElement address_test = chrome_driver_.findElement(By.name("us_address"));
        String input_text_address = address_test.getAttribute("value");
        Assert.assertEquals(input_text_address, player.address);

        WebElement phone_test = chrome_driver_.findElement(By.name("us_phone"));
        String input_text_phone = phone_test.getAttribute("value");
        Assert.assertEquals(input_text_phone, player.phone);

        WebElement submit_button = chrome_driver_.findElement(By.name("Submit"));

        submit_button.click();

        //delay();
    }

    public void edit_user(poker_player player)
    {
        WebElement email_test = chrome_driver_.findElement(By.name("us_email"));
        email_test.clear();
        email_test.sendKeys(player.email);

        WebElement first_name_test = chrome_driver_.findElement(By.name("us_fname"));
        first_name_test.clear();
        first_name_test.sendKeys(player.first_name);

        WebElement lname_test = chrome_driver_.findElement(By.name("us_lname"));
        lname_test.clear();
        lname_test.sendKeys(player.last_name);

        WebElement city_test = chrome_driver_.findElement(By.name("us_city"));
        city_test.clear();
        city_test.sendKeys(player.city);

        WebElement address_test = chrome_driver_.findElement(By.name("us_address"));
        address_test.clear();
        address_test.sendKeys(player.address);

        WebElement phone_test = chrome_driver_.findElement(By.name("us_phone"));
        phone_test.clear();
        phone_test.sendKeys(player.phone);

        WebElement submit_button = chrome_driver_.findElement(By.name("Submit"));

        submit_button.click();



        //delay();
    }

    public void delete_user(poker_player player)
    {
        WebElement login_for_search = chrome_driver_.findElement(By.name("login"));
        WebElement search_button = chrome_driver_.findElement(By.xpath("//button[text()='Search']"));

        login_for_search.sendKeys(player.login);

        search_button.click();

        //delay();
        WebDriverWait wait = new WebDriverWait(chrome_driver_, 10);
        WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[16]/a")));

        WebElement delete_button = chrome_driver_.findElement(By.xpath("//td[16]/a"));

        delete_button.click();


        //delay();

        Alert alert = chrome_driver_.switchTo().alert();
        alert.accept();


        WebElement dataTables_empty = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("dataTables_empty")));

        List<WebElement> no_data_label = chrome_driver_.findElements(By.className("dataTables_empty"));

        Assert.assertFalse(no_data_label.size() < 1);
    }

    public void logout(){
        WebElement logout = chrome_driver_.findElement(By.xpath("//div[1]/a/img"));
        logout.click();


    }

    public void close_browser()
    {
        chrome_driver_.quit();
        //chrome_driver_.close();
    }

//    public void delay()
//    {
//        chrome_driver_.manage().timeouts().implicitlyWait(200, SECONDS);
//
//    }

    private WebDriver chrome_driver_;
}
