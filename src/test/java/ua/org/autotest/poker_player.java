package ua.org.autotest;


import java.util.ArrayList;

public class poker_player
{
    String login;
    String email;
    String password_confirmed;
    String first_name;
    String last_name;
    String city;
    String address;
    String phone;

    public static String generate_random_name()
    {
        String symbols = "QWERTYUIOPASDFGHJKLZXCVBNMabcdefghijklmnopqrstuvwxyz0123456789";

        StringBuilder builder = new StringBuilder();

        int length = 3 + (int)(Math.random() * 7);

        while (length-- != 0)
        {
            int character = (int) (Math.random() * symbols.length());
            builder.append(symbols.charAt(character));
        }

        return builder.toString();
    }

    static public String generate_random_domain()
    {
        ArrayList<String> domains = new ArrayList<String>();

        domains.add("google.com");
        domains.add("yandex.ru");
        domains.add("gmail.com");
        domains.add("bing.com");
        domains.add("ukr.net");
        domains.add("yahoo.com");

        return domains.get((int) (Math.random() * domains.size()));
    }

    public static poker_player generate_player()
    {
        poker_player player = new poker_player();

        player.login = generate_random_name();
        player.email = generate_random_name() + "@" + generate_random_domain();
        player.password_confirmed = "12345QwE";
        player.first_name = generate_random_name();
        player.last_name = generate_random_name();
        player.city = generate_random_name();
        player.address = generate_random_name();
        player.phone = "0123456789";

        return player;
    }

}
