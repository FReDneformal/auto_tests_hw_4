package ua.org.autotest;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

public class base_test
{
    @BeforeSuite
    void setup()
    {
        WebDriverManager.chromedriver().setup();
        WebDriver web_driver = new ChromeDriver();

        tester_ = new poker_game_tester(web_driver);
    }

    @BeforeTest
    public void login()
    {
        poker_game_tester.website_credentials credentials = new poker_game_tester.website_credentials();
        credentials.login = site_config.login;
        credentials.password = site_config.password;

        tester_.open_website("http://qa-poker.teaminternational.com");
        tester_.login_user(credentials);
    }

    @AfterTest
    void logout(){ tester_.logout();
    }

    @AfterSuite
    void close_browser()
    {
        tester_.close_browser();
    }

    protected poker_game_tester tester_;
}








